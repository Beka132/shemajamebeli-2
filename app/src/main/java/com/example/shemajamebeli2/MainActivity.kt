package com.example.shemajamebeli2

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.core.graphics.red
import com.example.shemajamebeli2.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val names = ArrayList<String>()
        val ages = ArrayList<Int>()
        val lastNames = ArrayList<String>()
        val emails = ArrayList<String>()


        binding.btnAdd.setOnClickListener {
            if (binding.etName.text.isNotEmpty() && binding.etLastName.text.isNotEmpty() && binding.etAge.text.isNotEmpty() && binding.etEmail.text.isNotEmpty()) {
                if(!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString()).matches()){
                    Toast.makeText(this, "email is not valid", Toast.LENGTH_SHORT).show()
                }else {
                    binding.etName.setTextColor(Color.GREEN)
                    binding.etLastName.setTextColor(Color.GREEN)
                    binding.etAge.setTextColor(Color.GREEN)
                    binding.etEmail.setTextColor(Color.GREEN)
                    Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
                }
                if (names.contains(binding.etName.text.toString()) && lastNames.contains(binding.etLastName.text.toString()) && ages.contains(
                        binding.etAge.text.toString().toInt()
                    ) && emails.contains(binding.etEmail.text.toString())
                ) {
                    Toast.makeText(
                        this,
                        "User already exists",
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.etName.setTextColor(Color.RED)
                    binding.etLastName.setTextColor(Color.RED)
                    binding.etAge.setTextColor(Color.RED)
                    binding.etEmail.setTextColor(Color.RED)
                } else {
                    names.add(binding.etName.text.toString())
                    ages.add(binding.etAge.text.toString().toInt())
                    lastNames.add(binding.etLastName.text.toString())
                    emails.add(binding.etEmail.text.toString())
                }
            } else {
                binding.etName.setTextColor(Color.RED)
                binding.etLastName.setTextColor(Color.RED)
                binding.etAge.setTextColor(Color.RED)
                binding.etEmail.setTextColor(Color.RED)
                Toast.makeText(this, "Please fill all the fields given", Toast.LENGTH_SHORT).show()
            }

            binding.btnRemove.setOnClickListener {
                if (names.contains(binding.etName.text.toString()) && lastNames.contains(binding.etLastName.text.toString()) && ages.contains(
                        binding.etAge.text.toString().toInt()
                    ) && emails.contains(binding.etEmail.text.toString())){
                    names.remove(binding.etName.text.toString())
                    ages.remove(binding.etAge.text.toString().toInt())
                    lastNames.remove(binding.etLastName.text.toString())
                    emails.remove(binding.etEmail.text.toString())
                    Toast.makeText(this, "User deleted successfully", Toast.LENGTH_SHORT).show()
                    binding.etName.setTextColor(Color.GREEN)
                    binding.etLastName.setTextColor(Color.GREEN)
                    binding.etAge.setTextColor(Color.GREEN)
                    binding.etEmail.setTextColor(Color.GREEN)
                    } else{
                    Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
                    binding.etName.setTextColor(Color.RED)
                    binding.etLastName.setTextColor(Color.RED)
                    binding.etAge.setTextColor(Color.RED)
                    binding.etEmail.setTextColor(Color.RED)
                }
            }
            
            binding.btnUpdate.setOnClickListener {
                if (emails.contains(binding.etEmail.text.toString())){
                    var index=emails.indexOf(binding.etEmail.text.toString())
                    if (names.contains(binding.etName.text.toString())&&
                            lastNames.contains(binding.etLastName.text.toString())&&
                            ages.contains(binding.etAge.text.toString().toInt())){
                        Toast.makeText(this, "Please make a change", Toast.LENGTH_SHORT).show()
                        binding.etName.setTextColor(Color.RED)
                        binding.etLastName.setTextColor(Color.RED)
                        binding.etAge.setTextColor(Color.RED)
                        binding.etEmail.setTextColor(Color.RED)
                    }else {
                        names.set(index, binding.etName.text.toString())
                        lastNames.set(index, binding.etLastName.text.toString())
                        ages.set(index, binding.etAge.text.toString().toInt())
                        Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
                        binding.etName.setTextColor(Color.GREEN)
                        binding.etLastName.setTextColor(Color.GREEN)
                        binding.etAge.setTextColor(Color.GREEN)
                        binding.etEmail.setTextColor(Color.GREEN)
                    }
                } else{
                    Toast.makeText(this, "User does notexist", Toast.LENGTH_SHORT).show()
                    binding.etName.setTextColor(Color.RED)
                    binding.etLastName.setTextColor(Color.RED)
                    binding.etAge.setTextColor(Color.RED)
                    binding.etEmail.setTextColor(Color.RED)
                }

                ivExit.setOnClickListener{
                    onDestroy()
                }
                }
            fun onDestroy() {
                super.onDestroy()
            }
            }
        }
    }